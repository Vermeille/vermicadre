/*
 * Meteo.js
 * Copyright (C) 2014 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

var MeteoIcon = React.createClass({
    render: function() {
        var meteoIconUrl = 'http://openweathermap.org/img/w/'
                    + this.props.icon + '.png';
        return (<img className={this.props.className} src={meteoIconUrl} />);
    }
});

var MeteoTile = React.createClass({
    getMeteo: function() {
        $.getJSON('http://api.openweathermap.org/data/2.5/weather?q='
            + this.props.city + ',fr', function (data) {
            this.setState(data);
        }.bind(this));
    },
    getInitialState: function() {
        return {weather:[{ icon: "01d", main: "En attente"}]};
    },
    componentDidMount: function() {
        this.getMeteo();
        window.setInterval(this.getMeteo, this.props.refreshRate);
    },
    render: function() {
        return (
            <Tile id={"meteo-tile"}
                color={"bg-amber"}
                onClick={this.props.onClick}>
                <MeteoIcon icon={this.state.weather[0].icon}/>
            </Tile>

        );
    }
});

var Meteo = React.createClass({
    intervalId: 0,
    getMeteo: function() {
        if (!this.isMounted())
            return;
        var url = 'http://api.openweathermap.org/data/2.5/forecast?q='
            + this.props.city + ',fr&mode=json&units=metric&lang=fr';
        $.getJSON(url, function (data) {
            var filtered = [];
            var d = data.list;
            for (var i in d)
            {
                filtered.push({
                    desc: d[i].weather[0].description,
                    icon: d[i].weather[0].icon,
                    temp: d[i].main.temp,
                    when: d[i].dt_txt});
            }
            this.setState({data: filtered});
        }.bind(this));
    },

    getInitialState: function() {
        return {data: []};
    },

    componentDidMount: function() {
        this.getMeteo();
        this.intervalId = window.setInterval(this.getMeteo, this.props.refreshRate);
    },

    componentWillUnmount: function() {
        window.clearInterval(this.intervalId);
    },
    render: function() {
        var list = this.state.data.map(function(m) {
            return (
                <a href={'#'} className={'list'}>
                    <div className={'list-content'}>
                        <MeteoIcon icon={m.icon} className={'icon'} />
                        <div className={"data"}>
                            <span className={"list-title"}>{m.desc} ({m.temp}°)</span>
                            <span>{m.when}</span>
                        </div>
                    </div>
                </a>
                );
            });
        return (
            <div id={"meteo"} className={"bg-dark fg-white"}>
                <div
                    className={"listview-outlook"}
                    id={'meteo-list'}>
                    {list}
                </div>
            </div>
        );
    }
});

