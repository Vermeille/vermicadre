/*
 * Tile.js
 * Copyright (C) 2014 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

var Tile = React.createClass({
    onClick: function() {
        this.props.onClick(this.props.id);
    },
    render: function() {
        return (
            <div id={this.props.id}
                 className={"tile half " + this.props.color}
                 onClick={this.onClick} >
                 {this.props.children}
            </div>
        );
    }
});
