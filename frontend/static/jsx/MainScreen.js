/*
 * MainScreen.js
 * Copyright (C) 2014 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

var MainScreen = React.createClass({
    getInitialState: function() {
        return {main: 'gallery'};
    },

    setMain: function(mainId) {
        console.log('main set to ' + mainId);
        this.setState({main: mainId});
    },

    render: function() {
        switch (this.state.main)
        {
            case 'gallery':
                var main = <Carousel refreshRate={10000} />;
                break;
            case 'meteo-tile':
                var main = <Meteo city={"Toulon"} refreshRate={10000} />;
                break;
        }
        return (
            <div id={"mainscreen"}>
                {main}
                <MeteoTile
                    onClick={this.setMain}
                    refreshRate={10000}
                    city={"Toulon"} />

                <Tile
                    onClick={this.setMain}
                    id={"gallery"}
                    color={"bg-green"} >
                    <span className={"icon-pictures"} />
                </Tile>

                <Tile
                    onClick={poweroff}
                    id={"poweroff"}
                    color={"bg-red"} >
                    <span className={"icon-power"} />
                </Tile>
            </div>
        );
    }
});
