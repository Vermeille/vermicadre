/*
 * MainScreen.js
 * Copyright (C) 2014 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

var MainScreen = React.createClass({
    displayName: "MainScreen",
    getInitialState: function() {
        return {main: 'gallery'};
    },

    setMain: function(mainId) {
        console.log('main set to ' + mainId);
        this.setState({main: mainId});
    },

    render: function() {
        switch (this.state.main)
        {
            case 'gallery':
                var main = React.createElement(Carousel, {refreshRate: 10000});
                break;
            case 'meteo-tile':
                var main = React.createElement(Meteo, {city: "Toulon", refreshRate: 10000});
                break;
        }
        return (
            React.createElement("div", {id: "mainscreen"}, 
                main, 
                React.createElement(MeteoTile, {
                    onClick: this.setMain, 
                    refreshRate: 10000, 
                    city: "Toulon"}), 

                React.createElement(Tile, {
                    onClick: this.setMain, 
                    id: "gallery", 
                    color: "bg-green"}, 
                    React.createElement("span", {className: "icon-pictures"})
                )
            )
        );
    }
});
