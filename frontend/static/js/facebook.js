/*
 * facebook.js
 * Copyright (C) 2014 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
        $.get("/init?token=" + response.authResponse.accessToken + "&userid=" + response.authResponse.userID, function (data){ console.log('server init resp: '+data);});
    } else {
        document.getElementById('status').innerHTML = 'Please log ' + 'into this app.';
    }
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
    appId      : '479534292187493',
    xfbml      : true,
    version    : 'v2.2',
    cookie     : true
    });

    FB.login(function(data) { console.log(data); }, {scope: 'read_stream,user_photos' });

    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });

};

window.setInterval(function() {
    FB.login(function(data) { console.log(data); }, {scope: 'read_stream,user_photos' });

    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}, 1800*1000);

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/fr_FR/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

