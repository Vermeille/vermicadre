/*
 * Meteo.js
 * Copyright (C) 2014 vermeille <vermeille@Jadielle>
 *
 * Distributed under terms of the MIT license.
 */

var MeteoIcon = React.createClass({displayName: "MeteoIcon",
    render: function() {
        var meteoIconUrl = 'http://openweathermap.org/img/w/'
                    + this.props.icon + '.png';
        return (React.createElement("img", {className: this.props.className, src: meteoIconUrl}));
    }
});

var MeteoTile = React.createClass({displayName: "MeteoTile",
    getMeteo: function() {
        $.getJSON('/weather?city=' + this.props.city, function (data) {
            this.setState(data);
        }.bind(this));
    },
    getInitialState: function() {
        return {weather:[{ icon: "01d", main: "En attente"}]};
    },
    componentDidMount: function() {
        this.getMeteo();
        window.setInterval(this.getMeteo, this.props.refreshRate);
    },
    render: function() {
        return (
            React.createElement(Tile, {id: "meteo-tile", 
                color: "bg-amber", 
                onClick: this.props.onClick}, 
                React.createElement(MeteoIcon, {icon: this.state.weather[0].icon})
            )

        );
    }
});

var Meteo = React.createClass({displayName: "Meteo",
    intervalId: 0,
    getMeteo: function() {
        if (!this.isMounted())
            return;
        var url = '/forecast?city=' + this.props.city;
        $.getJSON(url, function (data) {
            console.log(data);
            var filtered = [];
            var d = data.list;
            for (var i in d)
            {
                filtered.push({
                    desc: d[i].weather[0].description,
                    icon: d[i].weather[0].icon,
                    temp: d[i].main.temp,
                    when: d[i].dt_txt});
            }
            this.setState({data: filtered});
        }.bind(this));
    },

    getInitialState: function() {
        return {data: []};
    },

    componentDidMount: function() {
        this.getMeteo();
        this.intervalId = window.setInterval(this.getMeteo, this.props.refreshRate);
    },

    componentWillUnmount: function() {
        window.clearInterval(this.intervalId);
    },
    render: function() {
        var list = this.state.data.map(function(m) {
            return (
                React.createElement("a", {href: '#', className: 'list'}, 
                    React.createElement("div", {className: 'list-content'}, 
                        React.createElement(MeteoIcon, {icon: m.icon, className: 'icon'}), 
                        React.createElement("div", {className: "data"}, 
                            React.createElement("span", {className: "list-title"}, m.desc, " (", m.temp, "°)"), 
                            React.createElement("span", null, m.when)
                        )
                    )
                )
                );
            });
        return (
            React.createElement("div", {id: "meteo", className: "bg-dark fg-white"}, 
                React.createElement("div", {
                    className: "listview-outlook", 
                    id: 'meteo-list'}, 
                    list
                )
            )
        );
    }
});

