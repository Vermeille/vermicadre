var Carousel = React.createClass({displayName: "Carousel",
    getImage: function() {
        if (!this.isMounted())
            return;
        $.getJSON('/photo', function (data) {
            console.log(data);
            this.setState(data);
        }.bind(this));
    },

    getInitialState: function() {
        return { url: "static/resources/01.jpg", caption: "Bienvenue!"};
    },

    intervalId: 0,
    componentDidMount: function() {
        this.getImage();
        this.intervalId = window.setInterval(this.getImage, this.props.refreshRate);
    },

    componentWillUnmount: function() {
        window.clearInterval(this.intervalId);
    },
    render: function() {
        return (
            React.createElement("div", {className: "image-container"}, 
                React.createElement("img", {src: this.state.url}), 
                React.createElement("div", {className: "overlay"}, 
                    React.createElement("span", null, this.state.caption)
                )
            )
       );
    }
});

