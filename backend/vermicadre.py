#!/usr/bin/env python

from flask import Flask, url_for, request, Response
import flask
import facebook
import random
import os
import requests

APP_ID = os.environ['APP_ID']
APP_SECRET = os.environ['APP_SECRET']

app = Flask(__name__, static_folder='/usr/src/app/frontend/static/', static_url_path='/static')

photos = []

FB = None

@app.route('/')
def index():
    return app.send_static_file('index.html')

def refresh_photos():
    global photos
    global FB
    photos = []
    after = ""
    while True:
        tmpphotos = FB.get_object('/me/photos/?fields=images,name,paging' + after)

        if len(tmpphotos['data']) == 0:
            break

        for p in tmpphotos['data']:
            photos.append({'url': p['images'][0]['source'], 'caption': p.get('name', '')})

        after = '&after=' + tmpphotos['paging']['cursors']['after']

@app.route('/init')
def hello_world():
    global FB
    FB = facebook.GraphAPI(request.args.get('token'))
    FB.extend_access_token(
            app_id=APP_ID,
            app_secret=APP_SECRET)
    refresh_photos()
    return 'OK';

@app.route('/photo')
def photo():
    if len(photos) == 0:
        return '{"url": "static/resources/01.jpg", "caption": "something went wrong"}'
    return flask.jsonify(**random.choice(photos))

@app.route('/weather')
def weather():
    res = requests.get('http://api.openweathermap.org/data/2.5/weather?q='
            + request.args.get('city') +
            ',fr&appid=f0dc4168c63da933dfd8866d69c6beaf').text
    r = app.response_class(
            response=res,
            status=200,
            mimetype='application/json')
    return r

@app.route('/forecast')
def forecast():
    res = requests.get('http://api.openweathermap.org/data/2.5/forecast?q='
            + request.args.get('city') +
            ',fr&appid=f0dc4168c63da933dfd8866d69c6beaf').text
    r = app.response_class(
            response=res,
            status=200,
            mimetype='application/json')
    return r

if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0", port=80)
